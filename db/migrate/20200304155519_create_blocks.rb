class CreateBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :blocks do |t| 
      t.integer :index
      t.datetime :timestamp
      t.string :data
      t.string :prev_hash
      t.string :current_hash
    end
  end
end
