require 'sinatra'
require 'sinatra/activerecord'
require 'digest'
require 'byebug'

class Block < ActiveRecord::Base

  # the first should be created
  def self.check_if_created
    if Block.any?
      true
    else
      Block.create(
        index: 0, 
        data: "Genesis", 
        prev_hash: "0",
        timestamp: Time.now, 
        hash: calculate_hash(0, Time.now, "Genesis", "0")
      )
    end
  end

  # check if valid
  def self.create_next(previous, data)
    index = previous.index + 1
    prev_hash = previous.hash
    timestamp = Time.now
    hash = self.calculate_hash(index, timestamp, data, prev_hash)
    
    block = Block.new(
      index: index, 
      data: data, 
      prev_hash: prev_hash, 
      timestamp: timestamp, 
      current_hash: hash
    )

    block.save
  end

  def self.calculate_hash(index, timestamp, data, prev_hash)
    sha = Digest::SHA256.new
    sha.update(index.to_s + timestamp.to_s + data.to_s + prev_hash.to_s)
    sha.hexdigest
  end
end
