require 'sinatra'
require 'ipfs-api'
require './app/block.rb'

get '/' do
  # ShowAllBlocks
  Block.all
end

get '/download_certain_block' do
  # DownloadCertainBlock
  ipfs = IPFS::Connection.new
  # retrieve the whole directory and make a copy of it in '/data'
  ipfs.get(params[:file_hash], 'data')
end

get '/check_first' do
  Block.check_if_created
end

get '/upload_new_block' do
  # UploadNewBlock
  ipfs = IPFS::Connection.new
  # Add to '/data'
  file = ipfs.add Dir.new('data')
  Block.create_block(Block.last, file[0].hash)
end
